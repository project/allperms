
-- SUMMARY --

The Drupal allperms module adds a 'Select All' option to each role on the
permissions edit form.


-- INSTALLATION --

Unpack the module to sites/*/modules and enable it via the modules admin page.


-- USAGE ---

Browse to http://example.com/?q=admin/user/permissions and check or uncheck
the 'Select All' box to select or disable all permissions for a specific role.


-- CAVEATS --

Uses jQuery, so won't work if you don't have javascript enabled in the browser.


-- CONTACT --

Current maintainers:
* Peter Lieverdink (cafuego) - http://drupal.org/user/218525
